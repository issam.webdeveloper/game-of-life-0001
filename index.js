const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');

const resolution = 30;


//var COLS = document.getElementById("largeur").value;
//var ROWS = document.getElementById("longeur").value;


function buildGrid() {
  const COLS = document.getElementById('largeur').value*1|5;
  const ROWS = document.getElementById('longeur').value*1|5;
  console.log(COLS, ROWS)
  return new Array(COLS).fill(null)
    .map(() => new Array(ROWS).fill(null)
      .map(() => Math.floor(Math.random() * 2)));
}

//render(grid);
function render () {
  const grid = buildGrid();
    for (let col = 0; col < grid.length; col++){
        for (let row = 0; row < grid[col].length; row++){
            const cell = grid[col][row];
            
            ctx.beginPath();
            ctx.rect(col * resolution, row * resolution, resolution, resolution);
            ctx.fillStyle = cell ? 'black' : 'white';
            ctx.fill();
            ctx.stroke();
        }
    }
}

